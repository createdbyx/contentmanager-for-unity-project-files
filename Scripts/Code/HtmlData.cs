﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.ContentManager.Scripts
{
    /// <summary>
    /// Provides a custom html data type.
    /// </summary>
    public class HtmlData
    {
        /// <summary>
        /// Gets or sets the html markup.
        /// </summary>
        public string Markup { get; set; }
    }
}
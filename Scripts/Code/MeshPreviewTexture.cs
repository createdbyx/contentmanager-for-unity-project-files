﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.ContentManager.Scripts.Code
{
    using UnityEngine;

    public class MeshPreviewTexture
    {
        public Texture2D Texture { get; set; }
    }
}
